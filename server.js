var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io').listen(http);

http.listen(3000, function(){
  console.log('listening on *:3000');
});

app.get('/', function (req, res) {
  res.sendfile('index.html');
});

io.sockets.on('connection', function (socket) {

	socket.on('conecta_sala', function(token){
		socket.nombre = token.nombre;
		socket.room = token.sala;
		socket.join(token.sala);
		console.log(token.sala, token.nombre);
		socket.broadcast.to(token.sala).emit('alerta', 'Usuario '+socket.nombre+'  conectado');
	});

	socket.on('envia', function (data) {
		io.sockets.in(socket.room).emit('actualiza', data);
	});

	socket.on('disconnect', function(){
		io.sockets.in(socket.room).emit('alerta', 'Usuario '+socket.nombre+' desconectado');
		socket.leave(socket.room);
	});
});